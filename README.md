Artificial Intelligence designed to win at a game called "Groker".
Designed for the first round of the national contest of informatics in Germany.
Uses decision trees to forecast the opponents behaviour.
This Project was created solely by Niels Mündler (nielstron)