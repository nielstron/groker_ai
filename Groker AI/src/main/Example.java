package main;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Eine Klasse um Beispielverl�ufe in einer Reihe zu verwalten
 * @author Niels M�ndler
 *
 */
public class Example
{
    /**
     * Die Wertemenge des Beispielinputs
     */
    private Map<String, Integer[]> row;
    /**
     * Die folgende Ver�nderung des gegnerischen Einsatzstapels
     */
    private Integer result;
    
    /**
     * Ein Beispiel f�r einen Input dessen Result berechnet werden soll
     * @param changes_opponent Die letzten �nderungen am Stapel des Gegeners
     * @param changes_self Die letzten �nderungen am eigenen Stapel
     * @param gain Der Gewinn der letzten Runden <br>
     * 		( 1 nur eigener, 0 beide, selber mehr, -1 beide, selber gleich viel oder weniger, -2 nur gegner)
     */
    public Example(Integer[] changes_opponent, Integer[] changes_self, Integer[] gain){
        // the result is unknown, therefore is result null
        this(changes_opponent, changes_self, gain, null);
    }
    
    /**
     * Ein Beispiel das bereits fertig gestellt ist
     * @param changes_opponent Die letzten �nderungen am Stapel des Gegeners
     * @param changes_self Die letzten �nderungen am eigenen Stapel
     * @param gain Der Gewinn der letzten Runden <br>
     * 		( 1 nur eigener, 0 beide, selber mehr, -1 beide, selber gleich viel oder weniger, -2 nur gegner)
     * @param result Die folgende �nderung des Gegners
     */
    public Example(Integer[] changes_opponent, Integer[] changes_self, Integer[] gain, Integer result){
        row = new HashMap<String, Integer[]>();
        row.put("changes_opponent", changes_opponent);
        row.put("changes_self", changes_self);
        // gain ist 1 bei nur selber gewonnen, 0 bei beide gewonnen (aber selber mehr), -1 beide gewonnen (aber selber weniger), -2 nur gegner gewonnen
        row.put("gain", gain);
        //the result is known
        this.result = result; 
    }
    
    /**
     *  Ein Beispiel-Beispiel
     */
    public Example(){
    	row = new HashMap<String, Integer[]>();
    	Integer[] tmp = {2,3,4,1,4};
        row.put("changes_opponent", tmp);
        Integer[] tmp1 = {2,4,1,4,2};
        row.put("changes_self", tmp1 );
        // gain ist 1 bei nur selber gewonnen, 0 bei beide gewonnen (aber selber mehr), -1 beide gewonnen (aber selber weniger), -2 nur gegner gewonnen
        Integer[] tmp2 = {1,0,-1,-2,1};
        row.put("gain", tmp2);
    }
    
    /**
     *  Ein Beispiel-Beispiel mit result
     */
    public Example(Integer result){
    	row = new HashMap<String, Integer[]>();
    	Integer[] tmp = {2,3,4,1,4};
        row.put("changes_opponent", tmp);
        Integer[] tmp1 = {2,4,1,4,2};
        row.put("changes_self", tmp1 );
        // gain ist 1 bei nur selber gewonnen, 0 bei beide gewonnen (aber selber mehr), -1 beide gewonnen (aber selber weniger), -2 nur gegner gewonnen
        Integer[] tmp2 = {1,0,-1,-2,1};
        row.put("gain", tmp2);
        this.result = result;
    }
    
    /**
     * Ein Beispiel dessen Werte bereits als HashMap vorliegen
     * @param row Die Map, die alle Werte mappt (String name => Integer[] Werte, an Stelle 0 der neueste Wert)
     * @param result Die folgende �nderung des Gegners
     */
    public Example( HashMap<String, Integer[]> row, Integer result){
    	this.row = row;
    	this.result = result;
    }
    
    /**
     * Ein Beispiel dessen Werte bereits als HashMap vorliegen, wobei das n�chste Zug noch nicht berechnet wurde
     * @param row Die Map, die alle Werte mappt (String name => Integer[] Werte, an Stelle 0 der neueste Wert)
     */
    public Example( HashMap<String, Integer[]> row){
    	this(row, null);
    }
    
    /**
     * Gibt die Reihe der Werte zur�ck
     * @return Die Reihe an Werten des Beispiels
     */
    public Map<String, Integer[]> getRow (){
    	return row;
    }
    
    /**
     * Gibt die n�chste �nderung des Gegners zur�ck
     * @return Die folgende �nderung des Gegners
     */
    public Integer getResult (){
    	return result;
    }
    
    /**
     * Ver�ndert die eingetragene folgende �nderung des Gegners
     * @param result Der neue Wert f�r die �nderung des Gegners
     */
    public void amendResult (Integer result){
    	this.result = result;
    }
    
    /**
     * Gibt Anhand eines Entscheidungsbaumes die auf die Beispielwerte folgende �nderung des Gegners zur�ck
     * @param tree Der entscheidende Baum
     * @return Die folgende �nderung des Gegners
     */
    public int getResultBy (DecisionTree tree){
    	return tree.getDecision(this);
    }
    
    /**
     * Speichert Anhand eines Entscheidungsbaumes die auf die Beispielwerte folgende �nderung des Gegners
     * @param tree Der entscheidende Baum
     * @return Die folgende �nderung des Gegners
     */
    public Integer computeResult (DecisionTree tree){
    	return result = getResultBy (tree);
    }
    
    /**
     * Gibt zur�ck ob das Ergebnis des Baumes bereits berechnet wurde
     * @return Wurde das Ergebenis berechnet?
     */
    public boolean isDone(){
    	// Wenn das result null ist wurde das Ergebnis noch nicht berechnet
    	return result != null;
    }
    
    @Override
	public String toString(){
    	String output = this.getClass().getName() + " { ";
    	for(String key : row.keySet()){
    		output += key + ": ";
    		output += Arrays.toString(row.get(key));
    		output += " ";
    	}
		output += "result: " +  result + " }";
    	return output;
    }
}
