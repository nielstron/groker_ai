package main;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;



public class Main {
	
	public static void main(String [ ] args)
	{
		/*Entscheidungsbaum neuer = new Entscheidungsbaum();
		neuer.return_top().add_zweig(new Status("<", -1), new Output(2));
		Entscheidungsregel tmp = new Entscheidungsregel(new Attribute("gain", 2));
		neuer.return_top().add_zweig(new Status(">", -1), tmp);


		System.out.println(new Example().toString());
		System.out.println(neuer.toString());
		System.out.println(neuer.gebeEntscheidung(new Example()));
		
		double[] array = {0.25, 0.25, 0.25, 0.25};
		System.out.println(AI.information(array));
		Vector<Integer> ergebnisse = new Vector<Integer>();
		ergebnisse.addElement(1);
		ergebnisse.addElement(1);
		ergebnisse.addElement(1);
		ergebnisse.addElement(1);
		ergebnisse.addElement(3);
		ergebnisse.addElement(3);
		ergebnisse.addElement(4);
		ergebnisse.addElement(4);
		ergebnisse.sort(null);
		System.out.println(ergebnisse);
		System.out.println(AI.information(ergebnisse));*/
		
		Vector<Example> examples = new Vector<Example>();
		for(int i = 0; i < 10; i++){
			examples.addElement(random_example(randomint(0, 6)));
		}
		for(int i = 0; i < 10; i++){
			examples.addElement(new Example(3));
		}
		Attribute best = AI.choose_best( Attribute.getAlleAttribute().toArray(new Attribute[0]), examples.toArray(new Example[0]));
		System.out.println(examples);
		System.out.println(best);
		System.out.println(AI.assign(best , examples.toArray(new Example[0])));
		Branch tree = AI.decision_tree_learning(examples, Attribute.getAlleAttribute(), 0);
		System.out.println(tree);
		System.out.println(AI.abstrahiere( AI.assign(best , examples.toArray(new Example[0])), (float)0.1));
		
		/*AI.neuer_stapel("self", 20);
		AI.neuer_stapel("self", 0);
		AI.neuer_stapel("self", 5);
		AI.neuer_stapel("self", 5);
		AI.neuer_stapel("self", 5);
		AI.neuer_stapel("opponent", 0);
		AI.neuer_stapel("opponent", 5);
		AI.neuer_stapel("opponent", 5);
		AI.neuer_stapel("opponent", 5);
		AI.neuer_stapel("opponent", 0);
		System.out.println(AI.gedaechtnis);
		System.out.println(AI.gedaechtnis.generate_output_example(6));*/
		
	}
	
	public static Integer[] random_array(int perspective){
		Integer[] ary = new Integer[perspective];
		for(int i = 0; i < perspective; i++){
			ary[i] = (randomint(-6, 6));
		}
		return ary;
	}
	
	public static Example random_example(int result){
		return new Example(random_array(Attribute.getPerspective()), random_array(Attribute.getPerspective()), random_array(Attribute.getPerspective()), result );
	}
	
	/**
	 * 
	 * @param min Minimalwert inclusive
	 * @param max Maximalwert inclusive
	 * @return eine zuf�llige Zahl zwischen min und max (inclusive)
	 */
	public static int randomint (Integer min, Integer max){
		//nextInt is normally exclusive of the top value,
		//so add 1 to make it inclusive
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
}
