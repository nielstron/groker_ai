package main;
import java.util.Arrays;

/**
 * Klasse um den Status von Zahlen zu verwalten<br>
 * Nach diesem Status wird entschieden ob der Zweig den der Status bildet weiter begangen wird zum Output oder zur n�chsten Regel
 * @author Niels M�ndler
 *
 */

public class Status implements Comparable<Status> {
	
	/**
	 * Die erste Vergleichszahl
	 */
	private Integer x0;
	/**
	 * Die zweite Vergleichszahl
	 */
	private Integer x1;
	/**
	 * Die Art des Status<br>
	 * 			"=" gleicht x0<br>
	 *			">" ist gr��er als x0<br>
	 *			"<" ist kleiner als x0<br>
	 *			">=" ist gr��er als oder gleicht x0<br>
	 *			"<=" ist kleiner als oder gleicht x0<br>
	 *			"!" ist ungleich x0<br>
	 *			"<>" liegt zwischen x0 und x1
	 */
	private String operator;
	/**
	 * Die m�glichen Arten des Status<br>
	 * 			"=" gleicht x0<br>
	 *			">" ist gr��er als x0<br>
	 *			"<" ist kleiner als x0<br>
	 *			">=" ist gr��er als oder gleicht x0<br>
	 *			"<=" ist kleiner als oder gleicht x0<br>
	 *			"!" ist ungleich x0<br>
	 *			"<>" liegt zwischen x0 und x1
	 */
	private static String[] possibilities = {
			"=",
			">",
			"<",
			">=",
			"<=",
			"!",
			"<>"
	};
	
	//Konstruktoren
	/**
	 * Erzeugt einen m�glichen Status einer Zahl<br>
	 * Ist der �bergebene Operator nicht Teil der m�glichen Operatoren so wird dem Status "=" als Operator zugewiesen
	 * @param set_operator Die Art des Status<br>
	 * 			"=" gleicht x0<br>
	 *			">" ist gr��er als x0<br>
	 *			"<" ist kleiner als x0<br>
	 *			">=" ist gr��er als oder gleicht x0<br>
	 *			"<=" ist kleiner als oder gleicht x0<br>
	 *			"!" ist ungleich x0<br>
	 *			"<>" liegt zwischen x0 und x1
	 * @param set_x0 Die erste Vergleichszahl x0
	 * @param set_x1 Die zweite Vergleichszahl x1
	 */
	public Status (String set_operator, Integer set_x0, Integer set_x1){
		x0 = set_x0;
		x1 = set_x1;
		if(Arrays.asList(possibilities).contains(set_operator)){
			operator = set_operator;
		}else operator = possibilities[0];
	}
	
	/**
	 * Erzeugt einen m�glichen Status einer Zahl<br>
	 * Ist der �bergebene Operator nicht Teil der m�glichen Operatoren so wird dem Status "=" als Operator zugewiesen
	 * @param set_operator Die Art des Status<br>
	 * 			"=" gleicht x0<br>
	 *			">" ist gr��er als x0<br>
	 *			"<" ist kleiner als x0<br>
	 *			">=" ist gr��er als oder gleicht x0<br>
	 *			"<=" ist kleiner als oder gleicht x0<br>
	 *			"!" ist ungleich x0<br>
	 *		"<>" liegt zwischen x0 und x1<br>
	 * @param set_x0 Die erste Vergleichszahl x0	
	 */
	public Status (String set_operator, Integer set_x0){
		//Konstruktor um anzugeben dass der wert gr��er oder kleiner oder gleich x0 sein muss
		this(set_operator, set_x0, null);
	}
	
	/**
	 * Erzeugt einen m�glichen Status einer Zahl<br>
	 * Der Operator ist automatisch "<>" (liegt zwischen)
	 * @param set_x0 Die erste Vergleichszahl x0
	 * @param set_x1 Die zweite Vergleichszahl x1
	 */
	public Status (int set_x0, int set_x1){
		//Konstruktor um anzugeben dass sich der wert zwischen x0 und x1 befinden soll
		this("<>", set_x0, set_x1);
	}
	
	/**
 	 * Erzeugt einen m�glichen Status einer Zahl<br>
	 * Der Operator ist automatisch "=" (gleicht)
	 * @param set_x0 Die erste Vergleichszahl x0
	 */
	public Status (Integer set_x0){
		this("=", set_x0);
	}
	
	/**
 	 * Erzeugt einen m�glichen Status einer Zahl<br>
	 * Der Status ist "= 0" (gleicht 0)
	 */
	public Status (){
		this("=", 0);
	}
	
	//Methoden
	
	/**
	 * Getter Methode um x0 des Status zu erhalten
	 * @return x0 des Status
	 */
	public Integer getX0(){
		return x0;
	}
	
	/**
	 * Getter Methode um x1 des Status zu erhalten
	 * @return x1 des Status
	 */
	public Integer getX1(){
		return x1;
	}
	
	/**
	 * Getter Methode um den Operator des Status zu erhalten
	 * @return Operator des Status
	 */
	public String getOperator(){
		return operator;
	}
	
	/**
	 * Gibt zur�ck ob die �bergebene Zahl den Status besitzt
	 * @param wert Die zu �berpr�fende Zahl
	 * @return Besitzt die Zahl den definierten Status?
	 */
	public boolean hasState (Integer wert){
		if(x0 != null){
			switch(operator){
			case "<>":
				if(x1 != null)
					return x0 < wert && x1 > wert;
			case "=":
				return wert == x0;
			case ">":
				return wert > x0;
			case "<":
				return wert < x0;
			case ">=":
				return wert >= x0;
			case "<=":
				return wert <= x0;
			case "!":
				return wert != x0;
			}
			return false;
		}
		else return wert == null;
	}
	
	public String toString(){
		String x1_string = "";
		
		if(x1 != null)  
			x1_string = x1.toString();
		
		return " ( " + operator + " " + x0.toString() + x1_string + " ) ";
	}
	
	public boolean isSimilar (Object other ){
		return this.toString() == other.toString();
	}
	
	public int compareTo (Status e1){
		return this.getX0() - e1.getX0();
	}
}
