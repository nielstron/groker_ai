package main;

/**
 * Eine abstrakte Klasse um als Stufe in einem Entscheidungsbaum zu dienen
 * 
 * @author Niels M�ndler
 */
public abstract class Branch
{
    /**
     * Gibt eine Entscheidung anhand seiner Entscheidungsregeln zur�ck
     * @param input Die Beispielwerte in Form eines Objekts der Klasse Example
     * @return Der berechnete Wert (folgende �nderung des Gegners) anhand der Entscheidungsregeln
     */
    public abstract Integer getValue (Example input);
}
