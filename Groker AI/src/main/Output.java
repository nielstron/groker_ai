package main;

/**
 * Ein Klasse die das Ende eines Entscheidungsastes darstellt, da ihr ein einfacher Wert zugewiesen ist, der bei Aufruf der Funktion <code>gebeWert</code> zur�ckgegeben wird.
 * 
 * @author Niels M�ndler
 *
 */
public class Output extends Branch
{
	/**
	 * Die Zahl die der Output ausgeben soll
	 */
    private Integer output;
    
    /**
     * Ein Output, der den Wert <code>set_output</output> zur�ckgibt wenn die Funktion <code>gebeWert</code> aufgerufen wird
     * @param set_output Der Wert der sp�ter bei Aufruf ausgegeben werden soll
     */
    public Output( int set_output){
        output = set_output;
    }
    
    /**
     * Ein Standart-Output, der <code>0</code> zur�ckgibt wenn die Funktion <code>gebeWert</code> aufgerufen wird
     */
    public Output(){
    	this(0);
    }
    
    /**
     * Gibt eine Entscheidung anhand seines definierten Outputs zur�ck
     * @param input Die Beispielwerte in Form eines Objekts der Klasse Example
     * @return Der berechnete Wert (folgende �nderung des Gegners) anhand der Entscheidungsregeln
     */
    public Integer getValue (Example input){
        return this.getValue();
    }
    
    /**
     * Gibt eine Entscheidung anhand seines definierten Outputs zur�ck
     * @return Der berechnete Wert (folgende �nderung des Gegners) anhand der Entscheidungsregeln
     */
    public Integer getValue (){
        return output;
    }
    
    public String toString(){
    	return " " + output.toString();
    }
    
    /**
     * Gibt zur�ck ob sich dieser Output und der Output <code>other</code> im R�ckgabewert gleichen
     * @param other Der andere Output
     * @return Gleichen sich die R�ckgabewerte?
     */
    public boolean gleicht ( Output other ) {
    	return other.getValue() == this.getValue();
    }
    
}
