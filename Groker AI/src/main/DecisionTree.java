package main;
/**
 * Eine Klasse um einen Entscheidungsbaum zu verwalten
 * 
 * @author Niels M�ndler
 */

public class DecisionTree {
    
	/**
	 * Die erste Entscheidungsregel des Baumes
	 */
    private DecisionRule top;
    
    /**
     * Standart-Konstruktor f�r den Entscheidungsbaum<br>
     * Definiert eine neue Standartentscheidungsregel an der Spitze
     */
    public DecisionTree (){
        this(new DecisionRule());
    }
    
    /**
     * Ein Entscheidungsbaum mit der Entscheidungsregel <code>set_top</code>
     * @param set_top Die erste Regel nach der Entschieden werden soll
     */
    public DecisionTree (DecisionRule set_top){
        top = set_top;
    }
    
    /**
     * Ein Entscheidungsbaum mit dem ersten entscheidenden Attribut <code>set_top</code>
     * @param set_top Das erste Attribut nach dem Entschieden werden soll
     */
    public DecisionTree (Attribute set_top){
        top = new DecisionRule(set_top);
    }
    
    /**
     * Gibt eine Entscheidung anhand seiner Entscheidungsregeln zur�ck
     * @param input Die Beispielwerte in Form eines Objekts der Klasse Example
     * @return Der berechnete Wert (folgende �nderung des Gegners) anhand der Entscheidungsregeln
     */
    public Integer getDecision ( Example input){
        return top.getValue(input);
    }
    
    /**
     * Gibt die erste Entscheidungsregel des Baumes zur�ck
     * @return Die erste Entscheidungsregel des Baumes
     */
    public DecisionRule returnTop(){
    	return top;
    }
    
    public String toString(){
    	return this.getClass().getName() + ":" + top.toString() ;
    }
    
}
