package main;
import java.util.*;

/**
 * Folgende Spielobjekte sind definiert:
 *
 * Spiel.Zustand.Spieler Spieler;
 *     
 *
 * Eigenschaften des Spieler-Spielobjekts:
 * Spieler.id() : int
 * Spieler.gewinnstapel() : int
 * Spieler.letzterZug() : int
 * Spieler.aktuellerZug() : int
 *
 * Zustandsmanipulation mit dem Spieler-Spielobjekt:
 * zustand.listeSpieler() : List<Spiel.Zustand.Spieler>
 *
 *
 * Sie koennen folgende Aktionen ausfuehren:
 *
 * zug.ausgabe(text : String) - Damit kannst du eine Debugausgabe machen. Verwende nicht System.out.println().
 *
 * zug.setzen(int anzahl);
 *     
 *
 *
 */
public class AI {

	public static Memory gedaechtnis = new Memory();
    /**
     * Hier implementiert ihr den Zug, den eure KI machen soll.
     * @param id eine eindeutige Zahl, die eure KI identifiziert
     * @param zustand schau in den oberen Kommentarblock
     * @param zug schau in den oberen Kommentarblock
     */
	
	
   public void zug(int id, Spiel.Zustand zustand, Spiel.Zug zug) {
       
	   Spiel.Zustand.Spieler self = zustand.listeSpieler().get(0);
	   Spiel.Zustand.Spieler opponent = zustand.listeSpieler().get(1);
	   neuer_stapel("self", self.letzterZug());
	   zug.ausgabe("Ich bot "+ self.letzterZug());
	   neuer_stapel("opponent", opponent.letzterZug());
	   zug.ausgabe("Der Gegner bot "+ opponent.letzterZug());
	   //Wenn wir seid mehr als 200 Runden Spielen, flushe das Ged�chtnis
	   /*if(gedaechtnis.get_past_rounds() >= 200){
		   gedaechtnis.flush();
	   }*/
	   //Erzeuge einen Entscheidungsbaum anhand der letzten Zust�nde
	   Vector<Example> past_situations = new Vector<>();
	   for(int i = 1; i < gedaechtnis.get_past_rounds(); i++){
		   past_situations.addElement(gedaechtnis.generate_output_example(i));
		   //zug.ausgabe("Die Situation vor " + i + " Runden war " + gedaechtnis.generate_output_example(i));
	   }
	   Branch dec_tree = AI.decision_tree_learning(past_situations, Attribute.getAlleAttribute(), 0);
	   //Entscheide die n�chste Ver�nderung am Stapel des Gegners
	   Example cur_situation = gedaechtnis.generate_input_example();
	   zug.ausgabe("Die aktuelle Situation ist " + cur_situation);
	   Integer next_change_opp = dec_tree.getValue(cur_situation);
	   zug.ausgabe("Die n�chste Berechnete �nderung des Gegeners an seinem Stapel ist " + next_change_opp);
	   //Und berechne seinen neuen Stapel
	   Integer old_stack_opp = gedaechtnis.read("stack_opponent", 1)[0];
	   Integer new_stack_opp = next_change_opp;
	   //Unterbiete den neuen Stapel des Gegners beim n�chsten zug um genau 6
	   Integer new_stack_self = new_stack_opp - 6;
	   //Aber setze nat�rlich mindestens 1
	   if(new_stack_self < 1) new_stack_self = 1;
	   //Sollte ich dabei nur weniger als 6 weniger als mein Gegner setzen, setze den exakten berechneten stapel des gegners + 5 (Durch Versuchen ermittelt)
	   if(new_stack_self > new_stack_opp - 5) new_stack_self = new_stack_opp + 5;
	   //Trotz allem immer noch mindestens 1
	   if(new_stack_self < 1) new_stack_self = 1;
	   zug.ausgabe("Daher biete ich n�chste Runde " + new_stack_self);
	   zug.setzen(new_stack_self);
    }
   
   /**
    * Funktion um die aktuellen Zugwerte an das Ged�chtnis zu �bergeben
    * @param whom Wessen Stapel?
    * @param howmuch Wie gro� ist der Stapel?
    */
   public static void neuer_stapel(String whom, Integer howmuch){
	   Integer old_stack = gedaechtnis.read("stack_"+whom, 1)[0];
	   if(old_stack == null) old_stack = 0;
	   Integer change = howmuch - old_stack;
	   gedaechtnis.feed("stack_"+whom, howmuch);
	   gedaechtnis.feed("changes_"+whom, change);
	   //result ist die Ver�nderung des Gegners in der folgenden Runde
   }

	/**
	 * Eine Funktion um Anhand einer Menge von Beispieleingaben und Attributen einen Entscheidungsbaum zu erstellen der m�glichst alle F�lle klassifiziert
	 * @param examples Ein Vector von Beispieleingaben
	 * @param attributes Ein Vector an Attributen ( Da dieser sp�ter ver�ndert/verringert werden muss)
	 * @param default_val Ein Standartausgabewert in einem unklassifizierten Fall
	 * @return Der gelernte Entscheidungsbaum
	 */
	public static Branch decision_tree_learning (Vector<Example> examples, Vector<Attribute> attributes, int default_val){
		// Falls es keine Beispiele mehr gibt
		if(examples.isEmpty()){
			// Gebe den default-Wert aus
			return new Output(default_val);
		}
		else if( AI.all_equal(AI.all_results_of(examples.toArray(new Example[0]))) ){
			return new Output(examples.elementAt(0).getResult());
		}
		// Falls es keine Attribute mehr gibt
		else if(attributes.isEmpty()){
			// Gebe den Mehrheitswert zur�ck (die am meisten vertretenen Ergebniss)
			return new Output( AI.majority_value( AI.all_results_of(examples.toArray( new Example[0] )).toArray(new Integer[0]) ) );
		}
		else{
			Attribute best = AI.choose_best (attributes.toArray(new Attribute[0]), examples.toArray(new Example[0]));
			DecisionRule tree = new DecisionRule(best, default_val);
			
			//Bestimme alle Stati die von best ausgehen
			Set<Status> states_of_best = AI.assign(best, examples.toArray(new Example[0])).keySet();
			for(Status state : states_of_best){
				
				//F�ge alle Beispiele die zu state geh�ren zusammen
				Vector<Example> examples_of_state = new Vector<>();
				for(Example ex : examples){
					if(state.hasState(best.value(ex))){
						examples_of_state.addElement(ex);
					}
				}
				//Entferne best aus den zu �begebenden Attributen
				attributes.remove(best);
				//Erzeuge einen neuen Standartwert aus dem Mehrheitswert der aktuellen Ergebnisse
				Integer new_default_val = AI.majority_value(AI.all_results_of(examples.toArray(new Example[0])).toArray(new Integer[0]));
				//Erzeuge einen neuen subtree durch Rekursion
				Branch subtree = decision_tree_learning (examples_of_state, attributes , new_default_val);
				//F�ge diesen Ast an den vorherigen Baum an
				tree.addBranch(state, subtree);
				
			}
			
			return tree;
		}
	}
	
	/**
	 * Funktion um das Attribut, das die besten Zusammenh�nge zwischen Attributwert und Ergebnis liefert und damit
	 * am Aussagekr�ftigsten ist, auszuw�hlen
	 * @param attributes Die Menge an m�glichen Attributen
	 * @param examples Die Menge an Beispielen
	 * @return Das aufschlussreichste Attribut
	 */
	public static Attribute choose_best(Attribute[] attributes, Example[] examples){
		// "Information" ist eigentlich das Gegenteil - die Ungewissheit
		
		// Berechne die aktuell vorhandene Information
		float current_information = 0;
		Vector<Integer> all_results = AI.all_results_of(examples);
		current_information += AI.information(all_results);
		
		// Berechne den Informationsgewinn f�r jedes Attribut
		// und speichere das bisher beste Attribut und dessen Informationsgewinn
		float max_information_gain = 100; // eine Unsicherheit von 100 wird nur bei 1267650600228229401496703205376 m�glichen Ergebnissen erreicht
		Attribute best = null;
		for(Attribute attr : attributes){
			Map<Status, Vector<Integer>> assignment = AI.assign(attr, examples);
			
			float missing_information = 0;
			for(Vector<Integer> values : assignment.values()){
				missing_information += ( values.size() / (float) examples.length ) * AI.information(values);
			}
			
			float information_gain = current_information - missing_information;
			
			if( information_gain < max_information_gain ){
				max_information_gain = information_gain;
				best = attr;
			}
		}
		
		return best;
	}
	
	/**
	 * Funktion um die Werte eines Attributs bei einer Menge von Beispielen einem Result zuzuordnen
	 * @param attr Das Attribut dessen Wert �berpr�ft werden soll
	 * @param examples Die Menge an Beispielen
	 * @return Eine Zurordnung von Wertstati und Results
	 */
	public static Map<Status, Vector<Integer>> assign (Attribute attr, Example[] examples){
		//die liste an Stati �ber die neue Outputs definiert werden
		Map<Status, Vector<Integer>> states = new HashMap<>();
		//F�r jedes Beispiel
		for(Example ex : examples){
			// erzeuge einen neuen Status ( = attributswert )
			Status tmp = new Status(attr.value(ex));
			// und definiere einen neuen Vector
			Vector<Integer> new_vec = new Vector<>();
			// wenn der Attributwert des Beispiel einen bereits gemappten Status hat
			for(Status state : states.keySet()){
				if(state.hasState(attr.value(ex))){
					tmp = state;
					// erhalte den gemappten vector 
					new_vec = states.get(tmp);
					break;
				}
			}
			// f�ge das Result von ex an den Vector an
			new_vec.addElement(ex.getResult());
			// und f�ge diesen Vector (wieder) auf den Status gemappt in die map ein
			states.put(tmp, new_vec);
		}
		return states;
	}
	
	/**
	 * Funktion um Stati zu verallgemeinern, also zu abstrahieren.
	 * @param states Eine Map, die bisherige Status-Result Verkn�pfungen speichert
	 * @param tolerance Wie viel Prozent der nicht zum Status geh�renden Results werden beim verallgemeinern toleriert?
	 * @return Eine Map, mit neuen, allgemeineren Status-Result Verkn�pfungen
	 * 
	 * TODO Daf�r sorgen dass das abstrahieren funktioniert
	 */
	public static Map<Status, Vector<Integer>> abstrahiere (Map<Status, Vector<Integer>> states, float tolerance){
		//Sammle alle Results dieser Map
		Set<Integer> all_results = new HashSet<>();
		for(Vector<Integer> vec : states.values()){
			all_results.addAll(vec);
		}
		//Sammle wirklich alle Results dieser Map
			Vector<Integer> truly_all_results = new Vector<>();
			for(Vector<Integer> vec : states.values()){
				truly_all_results.addAll(vec);
			}
		//Sammle alle "Gleichwerte" der Stati
		Set<Status> all_states = new HashSet<>();
		for(Status state : states.keySet()){
			all_states.add(state);
		}
		//Mappe f�r jedes Result die Stati, die dazu f�hren
		Map<Integer, Vector<Status>> paths = new HashMap<>();
		for(Integer result : all_results){
			for(Status state : states.keySet()){
				if(states.get(state).contains(result)){
					if(!paths.containsKey(result)){
						//Initialisiere die Liste, falls noch nicht geschehen
						paths.put(result, new Vector<Status>());
					}
					//F�ge den Status an der passt
					paths.get(result).addElement(state);
				}
			}
		}
		//System.out.println(paths);
		//Die map f�r die optimierten stati
		Map<Status, Vector<Integer>> new_states = new HashMap<>();
		
		//�berpr�fe f�r jedes Ergebnis ob es einen allgemeineren status f�r das result gibt
		for(Integer result : truly_all_results){
			//Soll noch weiter nach allgemeineren Stati gesucht werden?
			boolean continue_search = true;
			//wenn es mehr als einen weg gibt zu diesem result zu gelangen
			Set<Status> path_states = new HashSet<>();
			path_states.addAll(paths.get(result));
			//System.out.println(result + " " + path_states);
			if(path_states.size() > 1){
				//Sortiere alle Stati nach der gr��e
				Status[] path_states_array = path_states.toArray(new Status[0]); //hoffen wir dass es auch ohne klappt...
				Arrays.sort(path_states_array);
				//Suche alle results die �ber den selben status erreicht werden
				Vector<Integer> path_results = new Vector<>();
				for(Status state : path_states){
					if(path_states.contains(state))
						path_results.addAll(states.get(state));
				}
				Vector<Integer> rest_results = new Vector<>();
				rest_results.addAll(truly_all_results);
				rest_results.removeAll(path_results);
				//System.out.println(result + " " + rest_results);
				//Berechne den kleinsten wert �ber den zu result gelangt wird
				int min_value = path_states_array[0].getX0();
				//Berechne den gr��ten wert �ber den zu result gelangt wird
				int max_value = path_states_array[path_states_array.length - 1 ].getX0();
				//�berpr�fe f�r den minimalwert, und jeweils den n�chsten wert ob es einen verwendbaren status f�r den minimalwert gibt
				for(int i = min_value; i < max_value; i++ ){
					//Erzeuge einen Status, der aussagt dass eine zahl gr��er als der minimalwert sein soll und �berpr�fe seine verwendbarkeit
					Status abtract_state = new Status(">=", path_states_array[i].getX0());
					//liegt der anteil an falschen, ebenfalls zutreffenden results bei weniger als tolerance, mappe den status und breake
					if(AI.test_state(abtract_state, truly_all_results,  rest_results, tolerance)){
						Vector<Integer> results = new Vector<>(1);
						results.addElement(result);
						new_states.put(abtract_state, results);
						continue_search = false;
						break;
					}
				}
				if(!continue_search){
					continue;
				}
				//�berpr�fe f�r den maximalwert, und jeweils den vorherigen wert ob es einen verwendbaren status f�r den minimalwert gibt
				for(int i = max_value; i > min_value; i--){
					//Erzeuge einen Status, der aussagt dass eine zahl kleiner als der maximalwert sein soll und �berpr�fe seine verwendbarkeit
					Status abtract_state = new Status("<=", path_states_array[i].getX0());
					//liegt der anteil an falschen, ebenfalls zutreffenden results bei weniger als tolerance, mappe den status und breake
					if(AI.test_state(abtract_state, truly_all_results,  rest_results, tolerance)){
						Vector<Integer> results = new Vector<>(1);
						results.addElement(result);
						new_states.put(abtract_state, results);
						continue_search = false;
						break;
					}
				}
				if(!continue_search){
					continue;
				}
				//�berpr�fe f�r den maximalwert und minimalwert ob es einen verwendbaren status f�r den minimalwert und maximalwert gibt
				for(int i = min_value, j = max_value; i < j; i++, j-- ){
					//Erzeuge einen Status, der aussagt dass eine zahl zwischen minimalwert und maximalwert sein soll und �berpr�fe seine verwendbarkeit
					Status abtract_state = new Status("<>", path_states_array[i].getX0(), path_states_array[j].getX0());
					//liegt der anteil an falschen, ebenfalls zutreffenden results bei weniger als tolerance, mappe den status und breake
					if(AI.test_state(abtract_state, truly_all_results, rest_results, tolerance)){
						Vector<Integer> results = new Vector<>(1);
						results.addElement(result);
						new_states.put(abtract_state, results);
						continue_search = false;
						break;
					}
				}
				if(!continue_search){
					continue;
				}
			}
			//Wurde bis hier noch nichts gefunde, mappe aufden alten status
			for(Status state : path_states){
				Vector<Integer> results = new Vector<>();
				if(new_states.containsKey(state)){
					results = new_states.get(state);
				}
				results.addElement(result);
				new_states.put(state, results);
			}
		}
		
		return new_states;
	}
	
	/**
	 * Methode um zu �berpr�fen ob der Status test_state sinnvoll verwendbar ist
	 * @param abtract_state
	 * @param all_states
	 * @param rest_results
	 * @param tolerance
	 * @return
	 */
	private static boolean test_state(Status abtract_state, Vector<Integer> all_results,  Vector<Integer> rest_results, float tolerance) {
		int wrongs = 0;
		for(Integer result : rest_results){
			if(abtract_state.hasState(result)){
				wrongs++;
			}
		}
		//liegt der anteil an falschen, ebenfalls zutreffenden results bei weniger als tolerance, mappe den status und breake
		//System.out.println( wrongs + " " + path_states.size() +  abtract_state.toString() + ((float)wrongs / (float)path_states.size()));
		if(((float)wrongs / (float)all_results.size()) < tolerance ){
			return true;
		}
		return false;
	}

	/**
	 * Funktion um aus einer Menge von Warscheinlichkeiten von Ereignissen die in ihr enthaltene Information �ber das Eintreten der Ereignisse zu berechnen
	 * @param warscheinlichkeiten Die Menge an Warscheinlichkeiten P(vi) der Ereignisse v1 - vn
	 * @return Die in der Menge enthaltene Information
	 */
	public static double information (Double[] warscheinlichkeiten){
		double con_inf = 0;
		for(double poss : warscheinlichkeiten){
			con_inf += poss * -1 * (Math.log10(poss) / Math.log10(2));
		}
		return con_inf;
	}
	
	/**
	 * Funktion um aus einer Menge von Warscheinlichkeiten von Ereignissen die in ihr enthaltene Information �ber das Eintreten der Ereignisse zu berechnen<br>
	 * Kompatiblit�tsversion, mit int[] kompatibel
	 * @param warscheinlichkeiten Die Menge an Warscheinlichkeiten P(vi) der Ereignisse v1 - vn
	 * @return Die in der Menge enthaltene Information
	 */
	public static double information (double[] warscheinlichkeiten){
		// Cast from double[] to Double[]
		Double[] conv_ary = new Double[warscheinlichkeiten.length];
		for(int i = 0; i < warscheinlichkeiten.length; i++){
			conv_ary[i] =(Double) warscheinlichkeiten[i];
		}
		// Call the Integer[] version of majority_value
		return AI.information(conv_ary);
	}
	
	/**
	 * Funktion um aus einer Menge von Ereignissen die in ihr enthaltene Information �ber das Eintreten der Ereignisse zu berechnen<br>
	 * Kompatiblit�tsversion, mit einem Vector von Ergebnissen kompatibel
	 * @param warscheinlichkeiten Die Menge der Ereignisse
	 * @return Die in der Menge enthaltene Information
	 */
	public static double information (Vector<Integer> ergebnisse){
		// Z�hle das Vorkommen jeder Zahl
		HashMap<Integer, Integer> vorkommen = new HashMap<>();
		for (Integer result : ergebnisse){
			if(vorkommen.containsKey(result)){
				vorkommen.put(result, vorkommen.get(result) + 1);
			}
			else{
				vorkommen.put(result, 1);
			}
	    }
		Vector<Double> warscheinlichkeiten = new Vector<>();
		for (Integer anzahl : vorkommen.values()){
			warscheinlichkeiten.addElement( (double) anzahl / (double) ergebnisse.size());
		}
		return AI.information(warscheinlichkeiten.toArray(new Double[0]));
	}
	
	/**
	 * Eine Methode um den am meisten vertretenen Wert eines Arrays herauszufinden <br>
	 * <a href="https://stackoverflow.com/questions/1852631/determine-the-most-common-occurance-in-an-array">
	 * https://stackoverflow.com/questions/1852631/determine-the-most-common-occurance-in-an-array</a>
	 * @author dfa 
	 * @param ary Ein Array von Integern
	 * @return Der ( zuerst gefundene ) am meisten vertretene Wert
	 */
	public static int majority_value(Integer[] ary) {
	    Map<Integer, Integer> m = new HashMap<>();

	    for (int a : ary) {
	        Integer freq = m.get(a);
	        m.put(a, (freq == null) ? 1 : freq + 1);
	    }

	    int max = -1;
	    Integer mostFrequent = -1;

	    for (Map.Entry<Integer, Integer> e : m.entrySet()) {
	        if (e.getValue() > max) {
	            mostFrequent = e.getKey();
	            max = e.getValue();
	        }
	    }

	    return mostFrequent == null? 0 : mostFrequent;
	}
	
	/**
	 * Funktion um alle Results der �bergebenen Beispiele in einem Vector zur�ckzugeben
	 * @param examples Beispiele
	 * @return Alle Results der Beispiele in einem Vector
	 */
	public static Vector<Integer> all_results_of ( Example[] examples ){
		Vector<Integer> new_vec = new Vector<>();
		// f�ge alle Results der examples an den Vector an
		for(Example ex : examples){
			new_vec.addElement(ex.getResult());
		}
		return new_vec;
	}
	
	/**
	 * Funktion um anzugeben ob alle Objekte eines Vectors �bereinstimmen
	 * @param values Der Vector
	 * @return Sind alle Objekte dieselben?
	 */
	public static boolean all_equal ( Vector<Integer> values ){
		for(Integer value : values){
			if(value != values.firstElement()){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Eine Methode um den am meisten vertretenen Wert eines Arrays herauszufinden <br>
	 * Kompatiblit�tsversion, mit int[] kompatibel<br>
	 * <a href="https://stackoverflow.com/questions/1852631/determine-the-most-common-occurance-in-an-array">
	 * https://stackoverflow.com/questions/1852631/determine-the-most-common-occurance-in-an-array</a>
	 * @author Niels M�ndler
	 * @param ary Ein Array von Integern
	 * @return Der ( zuerst gefundene ) am meisten vertretene Wert
	 */
	public static int majority_value(int[] ary) {
		// Cast from int[] to Integer[]
		Integer[] conv_ary = new Integer[ary.length];
		for(int i = 0; i < ary.length; i++){
			conv_ary[i] =(Integer) ary[i];
		}
		// Call the Integer[] version of majority_value
		return AI.majority_value(conv_ary);
	}
}