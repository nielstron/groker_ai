package main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Klasse um die letzten Werte der Z�ge zu verwalten
 * 
 * @author Niels
 *
 */
public class Memory {

	/**
	 * Map um die Werte der letzten Z�ge zu speichern<br>
	 * Mappt Attibutnamen auf eine Liste von Integern (0 der letzte Zug, x der xte)
	 */
	private HashMap<String, List<Integer>> moves;
	
	/**
	 * Vector um die Results der entsprechenden Z�ge zu speichern<br>
	 * Result an Stelle x ist das Result vor x Runden
	 */
	private List<Integer> results;
	
	/**
	 * Erzeugt ein Memory-Objekt mit einer leeren Map
	 */
	public Memory (){
		moves = new HashMap<>();
	}
	
	
	/**
	 * F�gt den Wert eines Attributes an die Liste der vergangenen Werte vorne an<br>
	 * Der Wert f�r das Ergebnis muss zuletzt hinzugef�gt werden um eindeutige Verkn�pfung zu garantieren<br>
	 * Also Vorgehensweise: Erst alle Attributwerte feeden, dann den Resultwert
	 * @param attr_name Der Name des Attributes
	 * @param value Der Wert des Attributes in dieser Runde
	 */
	@SuppressWarnings("boxing")
	public void feed(String attr_name, int value){
		if(!moves.containsKey(attr_name)){
			moves.put(attr_name, new ArrayList<Integer>());
		}
		List<Integer> past = moves.get(attr_name);
		past.add(0, value);
		moves.put(attr_name, past);
	}
	
	/**
	 * Gibt die letzten <code>Attribute.perspective</code> Werte des Attributes zur�ck
	 * @param attr_name Der Name des Attributes
	 * @return Die letzten <code>Attribute.perspective</code> Werte des Attributes
	 */
	public Integer[] read(String attr_name){
		return this.read(attr_name, Attribute.getPerspective());
	}
	
	/**
	 * Gibt die letzten <code>perspective</code> Werte des Attributes zur�ck
	 * @param attr_name Der Name des Attributes
	 * @param perspective Die Menge der vergangenen Z�ge die betrachtet werden soll
	 * @return Die letzten <code>perspective</code> Werte des Attributes
	 */
	public Integer[] read(String attr_name, int perspective){
		return this.read(attr_name, perspective, 0);
	}
	
	/**
	 * Gibt <code>perspective</code> Werte des Attributes vor <code>past_shift</code> Runden zur�ck 
	 * @param attr_name Der Name des Attributes
	 * @param perspective Die Menge der vergangenen Z�ge die betrachtet werden soll
	 * @param past_shift Wieiviele Z�ge in der Vergangenheit sollen die Werte beginnen?
	 * @return Die letzten <code>perspective</code> Werte des Attributes
	 */
	public Integer[] read(String attr_name, int perspective, int past_shift){
		if(perspective <= 0) perspective = 1;
		Integer[] return_array = new Integer[perspective];
		if(!moves.containsKey(attr_name) || past_shift < 0 ||  past_shift > moves.get(attr_name).size() ){
			return return_array;
		}
		// if(perspective >= moves.get(attr_name).size()) st�rt nicht, da der rest einfach nicht �berschrieben wird (bleibt null) 
		// Das nachfolgende Konstrukt sichert, dass keine Werte abgefragt werden die au�erhalb der Liste liegen
		List<Integer> last_moves = moves.get(attr_name).subList(past_shift, moves.get(attr_name).size() );
		for(int i = 0; i < last_moves.size() && i < perspective; i++){
			return_array[i] = last_moves.get(i);
		}
		return return_array;
	}
	
	/**
	 * Generiert ein Beispielverlauf aufgrund der neuesten <code>perspective</code> Z�ge, ohne Result zur �bergabe an den Entscheidungsbaum
	 * @param perspective Die Menge der vergangenen Z�ge die betrachtet werden soll
	 * @return Der aktuelle Beispielverlauf
	 */
	public Example generate_input_example (int perspective){
		return generate_output_example(perspective, 0);
	}
	
	/**
	 * Generiert ein Beispielverlauf aufgrund der neuesten <code>Atribute.perspective</code> Z�ge, ohne Result zur �bergabe an den Entscheidungsbaum
	 * @return Der aktuelle Beispielverlauf
	 */
	public Example generate_input_example (){
		return generate_input_example(Attribute.getPerspective());
	}
	
	/**
	 * Generiert einen Beispielverlauf von vor <code>past_shift</code> Runden aufgrund der <code>perspective</code> vorherigen Z�ge, mit geloggten Results
	 * zur generierung eines Entscheidungsbaum
	 * @param past_shift Von vor wieviel Runden soll der Beispielverlauf sein?
	 * @param perspective Die Menge der vergangenen Z�ge die betrachtet werden soll
	 * @return Der Beispielverlauf vor <code>past_shift</code> Runden
	 */
	public Example generate_output_example (int perspective, int past_shift){

		HashMap<String, Integer[]> row = new HashMap<>();
		for(String attr : Attribute.getNamePossibilities()){
			row.put(attr, read(attr, perspective, past_shift));
		}
		
		//Das Result in der damaligen Runde ist der opp_change der n�chsten runde
		Integer result_then = read("changes_opponent", 1, past_shift - 1)[0];
		
		return new Example(row, result_then);
	}
	
	/**
	 * Generiert einen Beispielverlauf von vor <code>past_shift</code> Runden aufgrund der <code>Attribute.perspective</code> vorherigen Z�ge, mit geloggten Results
	 * zur generierung eines Entscheidungsbaum
	 * @param past_shift Von vor wieviel Runden soll der Beispielverlauf sein?
	 * @return Der Beispielverlauf vor <code>past_shift</code> Runden
	 */
	public Example generate_output_example ( int past_shift ){
		return this.generate_output_example(Attribute.getPerspective(), past_shift);
	}
	
	/**
	 * Generiert s�mtliche Examples
	 * @param perspective
	 * @return
	 */
	public Example[] generate_all_examples (int perspective){
		List<Example> table = new ArrayList<>();
		
		//Erzeuge f�r jede Runde (au�er der neuesten, warscheinlich resultlosen Runde) ein Example
		//Die Anzahl der geloggten Runden ist results.size()
		for(int i = 1; i < results.size();i++){
			table.add( generate_output_example(i) );
		}
		
		//Sollte die neueste Runde doch nicht Resultlos sein, so f�ge sie hinzu
		if(results.get(0) != null){
			table.add( generate_output_example(0) );
		}
		return table.toArray(new Example[0]);
	}
	
	public int get_past_rounds (){
		return moves.get(Attribute.getNamePossibilities()[0]).size();
	}
	
	public String toString(){
		return moves.toString();
	}

}
