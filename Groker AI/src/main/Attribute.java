package main;
import java.util.*;

/**
	 * Klasse um Attribute, nach denen Entscheidungsregeln entscheiden, zu verwalten
	 * 
	 * @author Niels M�ndler
	 */

public class Attribute {

/**
		 * Welche m�glichen Attributtypen gibt es?
		 */
		private static String[] name_poss = {
				"changes_opponent",
				"changes_self",
				"stack_opponent",
				"stack_self",
				//"gain"
		};
		/**
		 * Wie viele Ebenen in die Vergangenheit schaut das Programm?
		 * Und damit wie viele Attributnummern gibt es?
		 */
	    private static int perspective = 3; 
	    /**
	     * Der Name des zu �berpr�fenden Attributtyps
	     */
		private String name;
		/**
		 * Die Ebene in der Vergangenheit in der sich der Wert des Attributs befindet<br>
		 * 0 => letzter Zug, 5 => vor f�nf Z�gen
		 */
		private int number;
		
		/**
		 * Erzeugt ein Attribut anhand des Namens und der Stelle des Wertes im Beispiel
		 * @param set_name Der Name des Attributes
		 * @param set_number Die Stelle des Wertes des Attributes
		 */
		public Attribute (String set_name, int set_number){
			//der name des attributes muss einer der m�glichen sein
			if(Arrays.asList(name_poss).contains(set_name)){
				name = set_name;
			}else name = name_poss[0];
			//die nummer des attributes muss positiv sein, ist sie es nicht, wird sie es gemacht
			if(set_number < 0) set_number = - set_number;
			number = set_number;
		}
		
		/**
		 * Erzeugt ein Standartattribut mit den Werten <code>changes_opponent</code> und <code>0</code><br>
		 * Dieses Attribut verweist also auf die letzte (nullte) �nderung des Gegners an seinem Stapel
		 */
		public Attribute (){
			this ("changes_opponent", 0);
		}
		
		/**
		 * Gibt den Wert des Attributes in der Wertemenge des Beispiels zur�ck
		 * @param input Die Beispielwertemenge
		 * @return Der Wert des Attributes
		 */
		public Integer value (Example input){
			//Empfange die reihe aus dem Beispiel
			Map<String, Integer[]> row = input.getRow();
			
			//Extrahiere den relevanten wert aus dem Beispiel
			Integer example_value = 0;
			//wenn die nummer gr��er ist als die zahl der definierten zahlen wird 0 zur�ckgegeben
			if(row.containsKey(name) && number < row.get(name).length && number > 0){
				example_value = row.get(name)[number];
			}
			
			return example_value;
		}
		
		/**
		 * Gibt zur�ck ob das Attribut in seinen zu �berpr�fenden Werten einem anderem Attribut gleicht
		 * @param other Das andere Attribut
		 * @return Gleichen sich die zu �berpr�fenden Werte?
		 */
	    public boolean isSimilar(Attribute other){
	    	return this.toString().equals(other.toString());
	    }
		
	    /**
	     * Gibt alle m�glichen Attribute in einem Vector zur�ck<br>
	     * Dabei sind die Namen der Werttypen auf die in <code>name_poss</code> enthaltenen Namen beschr�nkt
	     * und die die Anzahl der Werte pro Typ auf die in <code>perspective</code> angegebene Anzahl
	     * @return Alle m�glichen Attribute
	     */
	    public static Vector<Attribute> getAlleAttribute(){
	    	Vector<Attribute> alle_attr = new Vector<Attribute>();
	    	for(String name : name_poss){
	    		for(int i = 0; i < perspective; i++){
	    			alle_attr.addElement(new Attribute(name, i));
	    		}
	    	}
	    	return alle_attr;
	    }
	    
		public String toString(){
			return name + " " + number;
		}
		
		public static int getPerspective (){
			return Attribute.perspective;
		}
		
		public static int setPerspective ( int setPerspective){
			Attribute.perspective = setPerspective;
			return Attribute.perspective;
		}
		
		public static String[] getNamePossibilities (){
			return Attribute.name_poss;
		}
	}