package main;
import java.util.HashMap;
import java.util.Map;

/**
 * Eine Klasse um die Regel zu verwalten nach der Entscheidungen im Baum getroffen werden
 * 
 * @author Niels M�ndler
 */
public class DecisionRule extends Branch
{
    /**
     * Das Attribut nach dem die Entscheidungsregel entscheidet
     */
    private Attribute attribute;
    /**
     * Eine Map an Zweigen<br>
     * Dabei ist jedem m�glichen Status einer Zahl ein "Zweig"
     * also ein Output oder eine weitere Entscheidungsregel zugewiesen auf
     * die dann weiter gegangen wird
     */
    private Map<Status, Branch> branches;
    /**
     * Ein Counter f�r die aktuell zu vergebende ID der Regeln
     */
    private static int cur_id = 0;
    /**
     * Die ID der Regel
     */
    private int id;
    
    //Konstruktoren
    
    /**
     * Default-Konstruktor<br>
     * Weist der Entscheidungsregel ein neues Standartattribut und eine inkrementierte ID zu
     */
    public DecisionRule(){
        this(new Attribute(), cur_id++);
    }
    
    /**
     * Eine Entscheidungsregel die das Attribut <code>set_attribute</code> verwendet<br>
     * Eine inkrementierte ID wird automatisch zugewiesen
     * @param set_attribute Das Attribut nach dem entschieden werden soll
     */
    public DecisionRule(Attribute set_attribute){
        this(set_attribute, cur_id++);
    }
    
    /**
     * Eine Entscheidungsregel mit der ID <code>set_id</code><br>
     * Ein Standartattribut wird automatisch zugewiesen<br>
     * Bei der Verwendung dieses Konstruktors verschwindet die Garantie auf Eindeutigkeit der ID-Objekt-Zuweisung
     * @param set_id Die ID der Regel
     */
    public DecisionRule(int set_id){
        this(new Attribute(), set_id);
    }
    
    /**
     * Eine Entscheidungsregel mit den angegebenen Attributen und der angegebenen ID<br>
     * Bei der Verwendung dieses Konstruktors verschwindet die Garantie auf Eindeutigkeit der ID-Objekt-Zuweisung
     * @param set_attribute Das Attribut nach dem entschieden werden soll
     * @param set_id Die ID der Regel
     */
    public DecisionRule(Attribute set_attribute, int set_id){
    	attribute = set_attribute;
    	branches = new HashMap<Status, Branch>();
    	id = set_id;
    }
    
    //Methoden
    
    /**
     * Gibt eine Entscheidung anhand seiner Entscheidungsregeln zur�ck<br>
     * Ist der Fall im Beispiel nicht definiert, so wird null zur�ckgegeben
     * @param input Die Beispielwerte in Form eines Objekts der Klasse Example
     * @return Der berechnete Wert (folgende �nderung des Gegners) anhand der Entscheidungsregeln
     */
    public Integer getValue( Example input){
    	//Erhalte den relevanten wert aus dem beispiel
    	int wert = attribute.value(input);
    	//F�r jeden Schl�ssel der Map (jeden abzweigenden Status)
        for(Status state : branches.keySet()){
        	//wenn der status zutrifft
        	if(state.hasState(wert)){
        		//gebe den wert des zweiges an auf den der status mappt
        		return branches.get(state).getValue(input);
        	}
        }
        //default r�ckgabe, wenn keiner der states zutrifft
        return null;
    }
    
    /**
     * F�gt einen neuen Fall zur Entscheidungsregel hinzu
     * @param new_status Der neue m�gliche Status des relevanten Attributs
     * @param new_branch Der neue Zweig zu dem der Status des relevanten Attributes f�hrt
     */
    public void addBranch( Status new_status, Branch new_branch){
    	//f�ge einen neuen Zweig zur regel hinzu
        branches.put(new_status, new_branch);
    }
    
    /**
     * Setzt die �bergebene Map als neue Zweige fest
     * @param set_branches Die neuen Zweige
     */
    public void setBranch( Map<Status, Branch> set_branches){
    	//f�ge einen neuen Zweig zur regel hinzu
        branches = set_branches;
    }
    
    /**
     * Gibt das relevante Attribut der Entscheidungsregel zur�ck
     * @return Das Attribut nach dem die Regel entscheidet
     */
    public Attribute getAttribute(){
    	return attribute;
    }
    
    /**
     * Gibt die ID der Regel zur�ck
     * @return Die ID der Regel
     */
    public int returnID(){
    	return id;
    }
    
    /**
     * Gibt zur�ck ob die Regel einer anderen Regel in ihrem Attribut gleicht
     * @param other Die andere Regel
     * @return Gleichen sich die Attribute?
     */
    public boolean isSimilar(DecisionRule other){
    	return this.attribute.isSimilar(other.getAttribute());
    }
    
    public String toString(){
    	return " " + this.getClass().getName() + " ( " + attribute.toString() + " ) " + branches.toString();
    }
}
